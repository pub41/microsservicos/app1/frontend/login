const criarSessao = (sessao) => {
    console.log('Sessão de usuário criada com sucesso!')
    localStorage.setItem('laravel-sso', JSON.stringify(sessao))
}

const recuperaSessao = () => {
   return JSON.parse(localStorage.getItem('laravel-sso'))
}

const removeSessao = () => {
    console.log('Sessão de usuário removida com sucesso!')
    return localStorage.removeItem('laravel-sso')
}

export default {
    criarSessao,
    recuperaSessao,
    removeSessao
}