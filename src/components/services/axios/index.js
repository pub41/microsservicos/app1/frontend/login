import axios from 'axios'

const instance = axios.create({
  baseURL: process.env.VUE_APP_SSO_URL,
  headers: {Authorization: ''}
});

/**
 *
 * @param path
 * @param params
 * @returns {Promise<AxiosResponse<T>>}
 */
const getRequest = (path = process.env.VUE_APP_SSO_URL, params = {}, headers = {}) => {
  return instance.get(path, params, headers).then(response => response).catch(error => error.response)
}

/**
 * @param path
 * @param payload
 * @returns {Promise<AxiosResponse<T>>}
 */
const postRequest = (path = process.env.VUE_APP_SSO_URL, payload, headers = {}) => {
  return instance.post(path, payload, headers).then(response => response).catch(error => error.response)
}

/**
 * @param path
 * @param id
 * @param payload
 * @returns {Promise<AxiosResponse<T>>}
 */
const putRequest = (path = process.env.VUE_APP_SSO_URL, id, payload) => instance.put(`${path}/${id}`, payload);

/**
 * @param path
 * @param id
 * @param payload
 * @returns {Promise<AxiosResponse<T>>}
 */
const deleteRequest = (path = process.env.VUE_APP_SSO_URL, id, payload = {}) => instance.delete(`${path}/${id}`, { data: payload });

export default {
  getRequest,
  postRequest,
  putRequest,
  deleteRequest
}